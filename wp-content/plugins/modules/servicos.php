<?php


/*
Plugin Name: Serviços
Plugin URI: http://www.dev-undergrounnd.com.br
Description: Serviços padrão
Version: 1.0
Author: DEV
Author URI: http://www.dev-undergrounnd.com.br
*/


/* serviços */
function servicos() {
	$labels = array(
		'name'                => 'Serviços',
		'singular_name'       => 'Serviços',
		'menu_name'           => 'Serviços',
		'parent_item_colon'   => 'Serviços:',
		'all_items'           => 'Todos os Serviços',
		'view_item'           => 'Ver Serviços',
		'add_new_item'        => 'Adicionar novo serviço',
		'add_new'             => 'Novo serviço',
		'edit_item'           => 'Editar serviço',
		'update_item'         => 'Atualizar serviço',
		'search_items'        => 'Serviços',
		'not_found'           => 'Nenhum serviço Encontrado',
		'not_found_in_trash'  => 'Nenhuma serviço Encontrado na Lixeira',
	);
	$rewrite = array(
		'slug'                => 'servico',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => false,
	);
	$args = array(
		'label'               => 'Serviços',
		'description'         => 'Serviços',
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),		
		'taxonomies'          => array( ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		//puxar o posttype para o menu e criar a sub sessão
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		#'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'query_var'           => 'servicos',
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);
	register_post_type( 'servicos', $args );
}

add_action( 'init', 'servicos', 0 );

